db_file = ARGV[0]

unless db_file
  puts "You need to specify database file"
else
  File.open(db_file).each do |line|
    db_name = line.split().first
    puts "purging: " + db_name
    `dropdb #{db_name}`
  end
end